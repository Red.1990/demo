<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
        //creer une variable repository et je vais dire a mon controller this fleche get doctrine je veux discuter avec doctrine et e veux un repository qui gere l'entity class article 
     
        $articles = $repo->findAll();


        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render('blog/home.html.twig');
    }

    
    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */

    public function form(Article $article = null, Request $request, ObjectManager $manager ){
  //si j'ai pas d'article !$article
    if(!$article){
    //une asstance de la classe article
        $article = new Article();

    }

       // $form = $this->createFormBuilder($article)//demande ç creer le form builder
         //     ->add('title', TextType::class)
          //    ->add('content', TextareaType::class)
           //   ->add('image', TextType::class)
              //on peut l'ajouter la bouton dans la page Create.html.twig             //->add('save', SubmitType::class, [
                //  'label' => 'Enregistrer'
             // ])
        //      ->getForm(); //apres avoir le resultat final pour afficher ce formulaire 
                    //il faut passer cet formulair a twig une(variable qui sera facile à afficher)  un tableau qui contiendra 
                      
             //traiter les info demander au formulair essaye d'analyser la requete
             //http que je te passe ici en paramètre 

             $form = $this->createForm(ArticleType::class, $article);
            $form->handleRequest($request);

            //la on enregister notre formulair on fait des conditions 
            //si le formulair est valides et les données sont bons
            //on vérifie si le fomulair est soumis et && est ce que le form est valide

            if($form->isSubmitted() && $form->isValid()){
            

               //si l'article a une date de création on le touchera pas 
               //on la modifier pas mais si l'article a pas une date on ajoute une date
                if(!$article->getId()){

                    $article->setCreatedAt(new \DateTime());
                }
                //donner ça date de creation

                //se preparer a fair persister
                $manager->persist($article);
                //balancer la requete
                $manager->flush();
                //ensuite redirige moi vers cette route

                return $this->redirectToRoute('blog_show', [
                    'id' => $article->getId()
                    ]);

            }

         return $this->render('blog/create.html.twig',[
             //on pass a twig un tableau qui contien un variable form
             //qui soit facile a afficher et qui va contenir le resultat de la fonction
             //createView de ce formulair, 
             //Parceque notre Class form avec toutes ses methodes ele a une methode 
             //qui s'appelle CreatView() qui va crer un petit objet qui represente l'aspet affichage  de notre formulaire
             'formArticle' => $form->createView(), 
             'editMode' =>$article->getId() !== null
         ]);
     }
     

    /**
     * @Route("/blog/{id}", name="blog_show")  
     */


    public function show(Article $article){


        return $this->render('blog/show.html.twig',[
            'article' => $article 
        ]);
    }
    //creer une formulaire 



}
